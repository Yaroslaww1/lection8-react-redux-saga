/* eslint-disable @typescript-eslint/no-namespace */
import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import { APP_PORT, ROUTES_WHITE_LIST, CORS_OPTIONS } from './config';
import routes from './api/routes';
import errorHandlerMiddleware from './api/middlewares/errorHandlerMiddleware';
import authorizationMiddleware from './api/middlewares/authorizationMiddleware';
import { IUser } from './data/models';

declare global {
  namespace Express {
    interface Request {
      user: IUser
    }
  }
}

const app = express();

app.use(bodyParser.json());
app.use(cors(CORS_OPTIONS));

app.use('/api/', authorizationMiddleware(ROUTES_WHITE_LIST));

routes(app);

app.use(errorHandlerMiddleware);

app.listen(APP_PORT, () => {
  console.log(`Server listening on port ${APP_PORT}!`);
});