import { Request, Response, NextFunction } from 'express';

export default (err: Error, req: Request, res: Response, next: NextFunction): void => {
  console.error(err);
  if (res.headersSent) {
    next(err);
  } else {
    const { message = '' } = err;
    res.status(500).send({ error: true, message });
  }
};
