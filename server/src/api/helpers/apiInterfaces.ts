import { IMessage, IUser } from "../../data/models"
import { IApiUser, IApiMessage } from "../interfaces"
import likeService from '../services/likeService';

export async function getIApiUser(user: IUser): Promise<IApiUser> {
  return user;
}

export async function getIApiMessage(message: IMessage, userId?: string): Promise<IApiMessage> {
  const newMessage = {
    ...message,
    isLiked: await likeService.getIsLiked({
      messageId: message.id,
      userId: userId || 'never be found'
    })
  }
  return newMessage;
}