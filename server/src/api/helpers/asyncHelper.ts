export async function asyncMap<T, K>(list: T[], asyncFunction: (item: T) => Promise<K>): Promise<K[]> {
  return Promise.all(list.map(item => asyncFunction(item)))
}