import lowdb from 'lowdb';
import { IDatabase, db } from '../../data/connection';
import { IMessage } from '../../data/models';

class MessageService {
  constructor(
    private db: lowdb.LowdbSync<IDatabase>
  ) {}

  async getAllMessages(): Promise<IMessage[]> {
    const messages = this.db.get('messages')
                          .value();
    return messages;
  }

  async getMessageById(messageId: string): Promise<IMessage> {
    const message = this.db.get('messages')
                            .find({ id: messageId })
                            .value();
    return message;
  }

  async addMessage(message: IMessage): Promise<IMessage> {
    this.db.get('messages')
            .push(message)
            .write();
    return message;
  }

  async updateMessageById(id: string, newMessage: IMessage): Promise<IMessage> {
    newMessage = {
      ...newMessage,
      editedAt: (new Date()).toISOString()
    }
    this.db.get('messages')
            .find({ id })
            .assign(newMessage)
            .write();
    const message = await this.getMessageById(id);
    return message;
  }

  async deleteMessageById(id: string): Promise<IMessage> {
    const message = this.db.get('messages')
                        .find({ id })
                        .value();
    db.get('messages')
      .remove({ id })
      .write();
    return message;
  }
}

const messageService = new MessageService(db);
export default messageService;