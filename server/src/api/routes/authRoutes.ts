import { Router, Request, Response, NextFunction } from 'express';
import asyncHandler from '../helpers/asyncHandler';
import userService from '../services/userService';
import authService from '../services/authService';
import { getIApiUser } from '../helpers/apiInterfaces';

const router = Router();

const loginUser = asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
  const { username, password } = req.body;
  const user = await userService.getUserByUsername(username);
  if (!user || user.password !== password)
    throw new Error('User with this credentials was not found');
  const { token } = await authService.addAuthToken(user);
  res.send({ user: await getIApiUser(user), token });
});

const getUser = asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
  const { user } = req;
  res.send({ user: await getIApiUser(user) });
});

router
  .post(
    '/login',
    loginUser
  )

  .post(
    '/user',
    getUser
  )

export default router;