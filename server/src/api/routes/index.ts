import express from 'express';
import messageRoutes from './messageRoutes';
import authRoutes from './authRoutes';
import userRoutes from './userRoutes';

export default (app: express.Application): void => {
  app.use('/api/messages', messageRoutes);
  app.use('/api/users', userRoutes);
  app.use('/api/auth', authRoutes);
};
