import { Router, Request, Response, NextFunction } from 'express';
import { v4 as uuid } from 'uuid';
import { asyncMap } from '../helpers/asyncHelper';
import { IUser } from '../../data/models';
import { getIApiUser } from '../helpers/apiInterfaces';
import asyncHandler from '../helpers/asyncHandler';
import { IApiUser } from '../interfaces';
import userService from '../services/userService';

const router = Router();

const getAllUsers = asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
  const oldUsers = await userService.getAllUsers()
  const users = await asyncMap<IUser, IApiUser>(oldUsers, user => getIApiUser(user));
  res.send({ users });
});

const deleteUser = asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params;
  const user = await userService.deleteUserById(id);
  res.send({ user: await getIApiUser(user) });
});

const getUser = asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params;
  const user = await userService.getUserById(id);
  res.send({ user: await getIApiUser(user) });
});

const addUser = asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
  const { username, password, avatar } = req.body;
  const newUser: IUser = {
    username,
    password,
    avatar,
    id: uuid(),
    isAdmin: false
  }
  const user = await userService.addUser(newUser);
  res.send({ user: await getIApiUser(user) });
});

const updateUser = asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params;
  let { ...user } = req.body;
  user = await userService.updateUserById(id, user);
  res.send({ user: await getIApiUser(user) });
});

router
  .get(
    '/',
    getAllUsers
  )

  .delete(
    '/:id',
    deleteUser
  )

  .get(
    '/:id',
    getUser
  )

  .post(
    '/',
    addUser
  )

  .put(
    '/:id',
    updateUser
  )

export default router;