/* eslint-disable prefer-const */
import { Router, Request, Response, NextFunction } from 'express';
import { v4 as uuid } from 'uuid';
import { getIApiMessage } from '../helpers/apiInterfaces';
import { asyncMap } from '../helpers/asyncHelper';
import asyncHandler from '../helpers/asyncHandler';
import messagesService from '../services/messagesService';
import { IMessage } from '../../data/models';
import { IApiMessage } from '../interfaces';
import likeService from '../services/likeService';
import userService from '../services/userService';

const router = Router();

const getAllMessages = asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params;
  const oldMessages = await messagesService.getAllMessages();
  if (!oldMessages)
    throw new Error('Messages were not found');
  const messages = await asyncMap<IMessage, IApiMessage>(oldMessages, message => getIApiMessage(message, id));
  messages.sort((m1: IApiMessage, m2: IApiMessage) => {
    const date1 = new Date(m1.createdAt);
    const date2 = new Date(m2.createdAt);
    return date1.getTime() - date2.getTime();
  });
  res.send({ messages });
});

const likeMessage = asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
  const { userId, messageId } = req.body;
  const oldMessage = await messagesService.getMessageById(messageId);
  if (!oldMessage)
    throw new Error('Message was not found');
  await likeService.addOrUpdateLike({ messageId, userId });
  const [message] = await asyncMap<IMessage, IApiMessage>([oldMessage], message => getIApiMessage(message, userId));
  res.send({ message });
});

const addMessage = asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
  const { text, userId } = req.body;
  const user = await userService.getUserById(userId);
  const newMessage: IMessage = {
    id: uuid(),
    text,
    user,
    createdAt: (new Date()).toISOString(),
    editedAt: ''
  };
  await messagesService.addMessage(newMessage);
  const [message] = await asyncMap<IMessage, IApiMessage>([newMessage], message => getIApiMessage(message, userId));
  res.send({ message });
});

const getMessage = asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params;
  const { userId } = req.body;
  let message = await messagesService.getMessageById(id);
  if (!message)
    throw new Error('Message was not found');
  const [_message] = await asyncMap<IMessage, IApiMessage>([message], message => getIApiMessage(message, userId));
  res.send({ message: _message });
});

const updateMessage = asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params;
  let { userId, ...message } = req.body;
  message = await messagesService.updateMessageById(id, message);
  if (!message)
    throw new Error('Message was not found');
  const [_message] = await asyncMap<IMessage, IApiMessage>([message], message => getIApiMessage(message, userId));
  res.send({ message: _message });
});

const deleteMessage = asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params;
  const message = await messagesService.deleteMessageById(id);
  if (!message)
    throw new Error('Message was not found');
  res.send({ message });
});

router
  .get(
    '/:id',
    getAllMessages
  )

  .get(
    '/message/:id',
    getMessage
  )

  .put(
    '/message/:id',
    updateMessage
  )

  .delete(
    '/message/:id',
    deleteMessage
  )

  .post(
    '/',
    addMessage
  )

  .post(
    '/likes',
    likeMessage
  )

export default router;