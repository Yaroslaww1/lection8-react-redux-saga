export interface IApiUser {
  id: string
  username: string
  avatar: string
  isAdmin: boolean
  password: string
}

export interface IApiMessage {
  id: string
  user: IApiUser
  text: string
  createdAt: string
  editedAt: string
  isLiked: boolean
}