import { SystemActions, SystemErrors, SystemActionTypes, SystemState } from './types';
import { isRequest, isError } from '../helpers/reduxHelper';

const initialState: SystemState = {
  isLoading: true,
  currentUser: null,
  error: '',
  isAuthorized: false
}

export function systemReducer(
  state = initialState,
  action: SystemActions
): SystemState {
  if (isRequest('SYSTEM', action.type)) 
    return {
      ...state,
      isLoading: true
    }
  if (isError('SYSTEM', action.type)) { 
    action = action as SystemErrors;
    return {
      ...state,
      error: action.error,
      isLoading: false
    }
  }
  switch (action.type) {
    case SystemActionTypes.FETCH_CURRENT_USER_SUCCESS: 
      return {
        ...state,
        isLoading: false,
        currentUser: action.user,
        isAuthorized: true
      }
    default:
      return state;
  }
}