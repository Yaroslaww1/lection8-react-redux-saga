import Api from '../helpers/apiHelper';
import { FETCH_USER_URL } from '../config';
import { all, call, put, takeEvery } from 'redux-saga/effects';
import { SystemActionTypes } from './types';
import { setCurrentUser, setError } from './actions';

export function* fetchCurrentUser() {
	try {
		const { response, error: errorMessage } = yield call(Api.post, FETCH_USER_URL);
		if (errorMessage) 
			throw new Error(errorMessage);	
		const { user } = response.data;
		yield put(setCurrentUser(user));
	} catch (error) {
		yield put(setError(error));
	}
}

function* watchFetchUser() {
	yield takeEvery(SystemActionTypes.FETCH_CURRENT_USER_REQUEST, fetchCurrentUser)
}

export default function* systemSagas() {
	yield all([
    watchFetchUser()
	])
};