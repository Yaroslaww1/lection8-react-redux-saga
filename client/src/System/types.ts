import { IUser } from "../interfaces";

export enum SystemActionTypes {
  FETCH_CURRENT_USER_REQUEST = 'SYSTEM/FETCH_CURRENT_USER_REQUEST',
  FETCH_CURRENT_USER_SUCCESS = 'SYSTEM/FETCH_CURRENT_USER_SUCCESS',
  FETCH_CURRENT_USER_ERROR = 'SYSTEM/FETCH_CURRENT_USER_ERROR'
}

interface IFetchCurrentUserRequestAction {
  type: SystemActionTypes.FETCH_CURRENT_USER_REQUEST
}

interface IFetchCurrentUserSuccessAction {
  type: SystemActionTypes.FETCH_CURRENT_USER_SUCCESS
  user: IUser
}

interface IFetchCurrentUserErrorAction {
  type: SystemActionTypes.FETCH_CURRENT_USER_ERROR
  error: string
}

type SystemRequests = IFetchCurrentUserRequestAction;
type SystemSuccess = IFetchCurrentUserSuccessAction;
export type SystemErrors = IFetchCurrentUserErrorAction;

export type SystemActions =
  SystemRequests
| SystemSuccess
| SystemErrors;

export interface SystemState {
  currentUser: IUser | null
  error: string
  isLoading: boolean
  isAuthorized: boolean
}