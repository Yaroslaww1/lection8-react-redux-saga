import React, { useEffect, useState } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { RouteComponentProps  } from 'react-router-dom';
import Button from '../../Components/Button';
import ErrorElement from '../../Components/Error';
import Spinner from '../../Components/Spinner';
import { IUser } from '../../interfaces';
import { RootState } from '../../store';
import { fetchUser, addUser, updateUser, setUser } from './actions';

import './styles.css';

const mapState = (state: RootState) => ({
  isLoading: state.userPage.isLoading,
  user: state.userPage.user,
  error: state.userPage.error
});

const mapDispatch = {
  fetchUser,
  addUser,
  updateUser,
  setUser
}

interface IMatchParams {
  id: string
}

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;
type Props = PropsFromRedux & RouteComponentProps<IMatchParams>;

const UserPage: React.FC<Props> = ({
  fetchUser: loadUser,
  isLoading,
  match,
  history,
  user: userProps,
  addUser: addUserProps,
  updateUser: updateUserProps,
  setUser: setUserProps,
  error
}) => {
  const [user, setUser] = useState<IUser>(userProps);
  useEffect(() => {
    if (match.params.id) {
      loadUser(match.params.id);
    } else {
      setUserProps({
        username: '',
        password: '',
        avatar: '',
        isAdmin: false,
        id: ''
      })
    }
  }, [match.params.id, setUserProps, loadUser]);

  useEffect(() => {
    setUser(userProps);
  }, [userProps])

  if (isLoading)
    return (
      <Spinner />
    )

  const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.currentTarget;
    setUser({
      ...user,
      [name]: value
    })
  }

  const onBack = () => {
    history.push('/users')
  }

  const onSave = () => {
    if (match.params.id) 
      updateUserProps(user)
    else
      addUserProps(user)
  }

  return (
    <div className="user-page-wrapper">
      <div className="content">
        <label>
          <div className="label">
            Username
          </div>
          <input
            id="username"
            value={user.username}
            onChange={onChange}
            name="username"
          />
        </label>
        <label>
          <div className="label">
            Password
          </div>
          <input
            id="password"
            value={user.password}
            onChange={onChange}
            name="password"
          />
        </label>
        <div className="buttons">
          <Button
            type='success'
            onClick={onSave}
          >
            {match.params.id ? 'Save' : 'Add'}
          </Button>
          <Button
            type='info'
            onClick={onBack}
          >
            Back
          </Button>
        </div>
        <ErrorElement
          error={error}
        />
      </div>
    </div>
  )
}

export default connector(UserPage);
