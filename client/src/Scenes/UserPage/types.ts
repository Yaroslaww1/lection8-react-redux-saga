import { IUser } from '../../interfaces';

export enum UserActionTypes {
  ERROR = 'UserActionTypes/ERROR',
  FETCH_USER_REQUEST = 'UserActionTypes/FETCH_USER_REQUEST',
  FETCH_USER_SUCCESS = 'UserActionTypes/FETCH_USER_SUCCESS',
  ADD_USER_REQUEST =   'UserActionTypes/ADD_USER_REQUEST',
  ADD_USER_SUCCESS =   'UserActionTypes/ADD_USER_SUCCESS',
  UPDATE_USER_REQUEST ='UserActionTypes/UPDATE_USER_REQUEST',
  UPDATE_USER_SUCCESS ='UserActionTypes/UPDATE_USER_SUCCESS',
}

export interface IFetchUserRequestAction {
  type: UserActionTypes.FETCH_USER_REQUEST
  userId: string
}

export interface IAddUserRequestAction {
  type: UserActionTypes.ADD_USER_REQUEST
  username: string
  password: string
  avatar: string
}

export interface IUpdateUserRequestAction {
  type: UserActionTypes.UPDATE_USER_REQUEST
  user: IUser
}

interface IFetchUserAction {
  type: UserActionTypes.FETCH_USER_SUCCESS
  user: IUser
}

interface IAddUserAction {
  type: UserActionTypes.ADD_USER_SUCCESS
  user: IUser
}

interface IUpdateUserAction {
  type: UserActionTypes.UPDATE_USER_SUCCESS
  user: IUser
}

interface IErrorAction {
  type: UserActionTypes.ERROR
  error: string
}

type UserRequests = 
  IFetchUserRequestAction
| IAddUserRequestAction
| IUpdateUserRequestAction;

type UserSuccess = 
  IFetchUserAction
| IAddUserAction
| IUpdateUserAction;

export type UserErrors = IErrorAction;

export type UserActions = 
  UserRequests
| UserSuccess
| UserErrors

export interface UserState {
  user: IUser
  error: string
  isLoading: boolean
}