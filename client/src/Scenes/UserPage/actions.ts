import { IUser } from "../../interfaces";
import { UserActions, UserActionTypes } from "./types";

export function fetchUser(userId: string): UserActions {
  return {
    type: UserActionTypes.FETCH_USER_REQUEST,
    userId
  }
}

export function addUser(obj: {username: string, password: string, avatar: string}): UserActions {
  return {
    type: UserActionTypes.ADD_USER_REQUEST,
    ...obj
  }
}

export function updateUser(user: IUser): UserActions {
  return {
    type: UserActionTypes.UPDATE_USER_REQUEST,
    user
  }
}


export function setUser(user: IUser): UserActions {
  return {
    type: UserActionTypes.FETCH_USER_SUCCESS,
    user
  }
}

export function setError(error: string): UserActions {
  return {
    type: UserActionTypes.ERROR,
    error
  }
}