import Api from '../../helpers/apiHelper';
import { USERS_URL } from '../../config';
import { all, call, put, takeEvery } from 'redux-saga/effects';
import { UserActionTypes, IAddUserRequestAction, IUpdateUserRequestAction, IFetchUserRequestAction } from './types';
import { setError, setUser } from './actions';
import { fetchUsers } from '../UsersListPage/actions';

function* fetchUserHandler(action: IFetchUserRequestAction) {
	try {
		const { response, error: errorMessage } = yield call(Api.get, `${USERS_URL}/${action.userId}`);
		
		if (errorMessage) 
			throw new Error(errorMessage);	

		const { user } = response.data;
		yield put(setUser(user));
	} catch (error) {
		yield put(setError(error.message));
	}
}

function* watchFetchUsers() {
	yield takeEvery(UserActionTypes.FETCH_USER_REQUEST, fetchUserHandler)
}

function* addUserHandler(action: IAddUserRequestAction) {
	try {
		const { type, ...user } = action;
		const { response, error: errorMessage } = yield call(Api.post, USERS_URL, user);
		
		if (errorMessage) 
			throw new Error(errorMessage);	

		const { user: newUser } = response.data;
		yield put(setUser(newUser));
		yield put(fetchUsers());
	} catch (error) {
		yield put(setError(error.message));
	}
}

function* watchAddUser() {
	yield takeEvery(UserActionTypes.ADD_USER_REQUEST, addUserHandler)
}

function* updateUserHandler(action: IUpdateUserRequestAction) {
	try {
		const { response, error: errorMessage } = yield call(Api.put, `${USERS_URL}/${action.user.id}`, action.user);
		
		if (errorMessage) 
			throw new Error(errorMessage);	

		const { user } = response.data;
		yield put(setUser(user));
		yield put(fetchUsers());
	} catch (error) {
		yield put(setError(error.message));
	}
}

function* watchUpdateUser() {
	yield takeEvery(UserActionTypes.UPDATE_USER_REQUEST, updateUserHandler)
}

export default function* userPageSagas() {
	yield all([
		watchFetchUsers(),
		watchAddUser(),
		watchUpdateUser()
	])
};