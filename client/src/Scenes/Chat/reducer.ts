import { isRequest, isError } from '../../helpers/reduxHelper';
import { ChatActions, ChatActionTypes, ChatErrors, ChatState } from './types';

const initialState: ChatState = {
  isLoading: true,
  messages: [],
  error: ''
}

export function chatReducer(
  state = initialState,
  action: ChatActions
): ChatState {
  if (isRequest('ChatActionTypes', action.type)) 
    return {
      ...state,
      isLoading: true
    }
  if (isError('ChatActionTypes', action.type)) { 
    action = action as ChatErrors;
    return {
      ...state,
      error: action.error,
      isLoading: false
    }
  }
  switch (action.type) {
    case ChatActionTypes.FETCH_MESSAGES_SUCCESS:
      return {
        ...state,
        isLoading: false,
        messages: action.messages
      }
    default:
      return state;
  }
}