import { IMessage } from "../../interfaces";

export enum ChatActionTypes {
  ERROR = 'ChatActionTypes/ERROR',
  FETCH_MESSAGES_REQUEST = 'ChatActionTypes/FETCH_MESSAGES_REQUEST',
  FETCH_MESSAGES_SUCCESS = 'ChatActionTypes/FETCH_MESSAGES_SUCCESS',
  LIKE_MESSAGE_REQUEST =   'ChatActionTypes/LIKE_MESSAGE_REQUEST',
  LIKE_MESSAGE_SUCCESS =   'ChatActionTypes/LIKE_MESSAGE_SUCCESS',
  ADD_MESSAGE_REQUEST =   'ChatActionTypes/ADD_MESSAGE_REQUEST',
  ADD_MESSAGE_SUCCESS =   'ChatActionTypes/ADD_MESSAGE_SUCCESS',
}

export interface IFetchMessagesRequestAction {
  type: ChatActionTypes.FETCH_MESSAGES_REQUEST
  userId: string
}

export interface ILikeMessageRequestAction {
  type: ChatActionTypes.LIKE_MESSAGE_REQUEST
  messageId: string
  userId: string
}

export interface IAddMessageRequestAction {
  type: ChatActionTypes.ADD_MESSAGE_REQUEST
  text: string
  userId: string
}

interface IFetchMessagesAction {
  type: ChatActionTypes.FETCH_MESSAGES_SUCCESS
  messages: IMessage[]
}

interface ILikeMessageAction {
  type: ChatActionTypes.LIKE_MESSAGE_SUCCESS
}

interface IAddMessageAction {
  type: ChatActionTypes.ADD_MESSAGE_SUCCESS
}

interface IErrorAction {
  type: ChatActionTypes.ERROR
  error: string
}

type ChatRequests = 
  IFetchMessagesRequestAction
| ILikeMessageRequestAction
| IAddMessageRequestAction;

type ChatSuccess = 
  IFetchMessagesAction
| ILikeMessageAction
| IAddMessageAction;

export type ChatErrors = IErrorAction;

export type ChatActions = 
  ChatRequests
| ChatSuccess
| ChatErrors

export interface ChatState {
  messages: IMessage[]
  error: string,
  isLoading: boolean
}