import { IMessage } from "../../interfaces";
import { MessageActions, MessageActionTypes } from "./types";

export function fetchMessage(messageId: string, userId: string): MessageActions {
  return {
    type: MessageActionTypes.FETCH_MESSAGE_REQUEST,
    messageId,
    userId
  }
}

export function updateMessage(message: IMessage, userId: string): MessageActions {
  return {
    type: MessageActionTypes.UPDATE_MESSAGE_REQUEST,
    message,
    userId
  }
}

export function deleteMessage(messageId: string, userId: string): MessageActions {
  return {
    type: MessageActionTypes.DELETE_MESSAGE_REQUEST,
    messageId,
    userId
  }
}

export function setMessage(message: IMessage): MessageActions {
  return {
    type: MessageActionTypes.FETCH_MESSAGE_SUCCESS,
    message
  }
}

export function setError(error: string): MessageActions {
  return {
    type: MessageActionTypes.ERROR,
    error
  }
}