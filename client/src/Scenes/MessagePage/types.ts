import { IMessage } from '../../interfaces';

export enum MessageActionTypes {
  ERROR = 'MessageActionTypes/ERROR',
  FETCH_MESSAGE_REQUEST = 'MessageActionTypes/FETCH_MESSAGE_REQUEST',
  FETCH_MESSAGE_SUCCESS = 'MessageActionTypes/FETCH_MESSAGE_SUCCESS',
  UPDATE_MESSAGE_REQUEST ='MessageActionTypes/UPDATE_MESSAGE_REQUEST',
  UPDATE_MESSAGE_SUCCESS ='MessageActionTypes/UPDATE_MESSAGE_SUCCESS',
  DELETE_MESSAGE_REQUEST ='MessageActionTypes/DELETE_MESSAGE_REQUEST',
  DELETE_MESSAGE_SUCCESS ='MessageActionTypes/DELETE_MESSAGE_SUCCESS',
}

export interface IFetchMessageRequestAction {
  type: MessageActionTypes.FETCH_MESSAGE_REQUEST
  messageId: string
  userId: string
}

export interface IUpdateMessageRequestAction {
  type: MessageActionTypes.UPDATE_MESSAGE_REQUEST
  message: IMessage
  userId: string
}

export interface IDeleteMessageRequestAction {
  type: MessageActionTypes.DELETE_MESSAGE_REQUEST
  messageId: string
  userId: string
}

interface IFetchMessageAction {
  type: MessageActionTypes.FETCH_MESSAGE_SUCCESS
  message: IMessage
}

interface IUpdateMessageAction {
  type: MessageActionTypes.UPDATE_MESSAGE_SUCCESS
  message: IMessage
}

interface IDeleteMessageAction {
  type: MessageActionTypes.DELETE_MESSAGE_SUCCESS
  message: IMessage
}

interface IErrorAction {
  type: MessageActionTypes.ERROR
  error: string
}

type MessageRequests = 
  IFetchMessageRequestAction
| IUpdateMessageRequestAction
| IDeleteMessageRequestAction;

type MessageSuccess = 
  IFetchMessageAction
| IUpdateMessageAction
| IDeleteMessageAction;

export type MessageErrors = IErrorAction;

export type MessageActions = 
  MessageRequests
| MessageSuccess
| MessageErrors

export interface MessageState {
  message: IMessage
  error: string
  isLoading: boolean
}