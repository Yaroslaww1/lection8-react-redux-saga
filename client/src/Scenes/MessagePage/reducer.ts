import { MessageErrors, MessageActions, MessageActionTypes, MessageState } from './types';
import { isRequest, isError } from '../../helpers/reduxHelper';
import { getDefaultMessage } from '../../Services/messageService';

const initialState: MessageState = {
  message: getDefaultMessage(),
  isLoading: false,
  error: ''
}

export function messagePageReducer(
  state = initialState,
  action: MessageActions
): MessageState {
  if (isRequest('MessageActionTypes', action.type)) 
    return {
      ...state,
      isLoading: true
    }
  if (isError('MessageActionTypes', action.type)) { 
    action = action as MessageErrors;
    return {
      ...state,
      error: action.error,
      isLoading: false
    }
  }
  switch (action.type) {
    case MessageActionTypes.FETCH_MESSAGE_SUCCESS:
      return {
        ...state,
        isLoading: false,
        message: action.message
      }
    default:
      return state;
  }
}