import { LoginActions, LoginActionTypes } from "./types";

export function loginUser(username: string, password: string): LoginActions {
  return {
    type: LoginActionTypes.LOGIN_USER,
    username,
    password
  }
}

export function setLoginError(error: string): LoginActions {
  return {
    type: LoginActionTypes.LOGIN_USER_ERROR,
    error
  }
}