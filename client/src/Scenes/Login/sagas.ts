import Api from '../../helpers/apiHelper';
import { LOGIN_URL } from '../../config';
import { all, call, put, takeEvery } from 'redux-saga/effects';
import { LoginActionTypes, IFetchCurrentUserAction } from './types';
import { setCurrentUser } from '../../System/actions';
import { setLoginError } from './actions';

export function* loginUser(action: IFetchCurrentUserAction) {
	try {

		const { username, password } = action;
		const { response, error: errorMessage } = yield call(Api.post, LOGIN_URL, { username, password });
		
		if (errorMessage) 
			throw new Error(errorMessage);	

		const { user, token } = response.data;
		localStorage.setItem('token', token);
		yield put(setCurrentUser(user));
	} catch (error) {
		yield put(setLoginError(error.message));
	}
}

function* watchLoginUser() {
	yield takeEvery(LoginActionTypes.LOGIN_USER, loginUser)
}

export default function* loginPageSagas() {
	yield all([
    watchLoginUser()
	])
};