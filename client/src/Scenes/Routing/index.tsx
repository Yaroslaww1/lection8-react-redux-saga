import React, { useEffect } from 'react';
import { Switch } from 'react-router-dom';
import { connect, ConnectedProps } from 'react-redux';
import { RootState } from '../../store';
import PrivateRoute from '../../Containers/PrivateRoute';
import AdminRoute from '../../Containers/AdminRoute';
import HomePage from '../Home';
import LoginPage from '../Login';
import UsersListPage from '../UsersListPage';
import PublicRoute from '../../Containers/PublicRoute';
import { fetchCurrentUser } from '../../System/actions';
import Spinner from '../../Components/Spinner';
import UserPage from '../UserPage';
import MessagePage from '../MessagePage';

const mapState = (state: RootState) => ({
  isAuthorized: state.system.isAuthorized,
  isLoading: state.system.isLoading
});

const mapDispatch = {
  fetchCurrentUser
}

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;
type Props = PropsFromRedux;

const Routing: React.FC<Props> = ({
  isAuthorized,
  isLoading,
  fetchCurrentUser: loadUser
}) => {
  useEffect(() => {
    if (!isAuthorized) {
      loadUser();
    }
  }, [isAuthorized, loadUser]);

  if (isLoading) {
    return (
      <Spinner />
    )
  }

	return (
		<div className="App">
			<Switch>
        <PublicRoute exact path="/login" component={LoginPage} />
				<PrivateRoute exact path="/" component={HomePage} />
        <PrivateRoute path="/message/:id" component={MessagePage} />
        <AdminRoute exact path="/users" component={UsersListPage} />
        <AdminRoute exact path="/user" component={UserPage} />
        <AdminRoute path="/user/:id" component={UserPage} />
        {/* <PrivateRoute path="/messages/:id" component={MessagePage} />
				<AdminRoute exact path="/user" component={UserPage} />
				<AdminRoute path="/user/:id" component={UserPage} />
        <AdminRoute exact path="/users" component={UsersListPage} /> */}
			</Switch>
		</div>
	);
}

export default connector(Routing);