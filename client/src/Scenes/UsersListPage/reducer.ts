import { UserListErrors, UserListActions, UserListActionTypes, UserListState } from './types';
import { isRequest, isError } from '../../helpers/reduxHelper';

const initialState: UserListState = {
  users: [],
  error: '',
  isLoading: true
}

export function usersListPageReducer(
  state = initialState,
  action: UserListActions
): UserListState {
  if (isRequest('UserListActionTypes', action.type)) 
    return {
      ...state,
      isLoading: true
    }
  if (isError('UserListActionTypes', action.type)) { 
    action = action as UserListErrors;
    return {
      ...state,
      error: action.error,
      isLoading: false
    }
  }
  switch (action.type) {
    case UserListActionTypes.FETCH_USERS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        users: action.users
      }
    default:
      return state;
  }
}