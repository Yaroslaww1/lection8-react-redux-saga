import Api from '../../helpers/apiHelper';
import { USERS_URL } from '../../config';
import { all, call, put, takeEvery } from 'redux-saga/effects';
import { UserListActionTypes, IDeleteUserRequestAction } from './types';
import { fetchUsers, setError, setUsers } from './actions';

function* fetchUsersHandler() {
	try {
		const { response, error: errorMessage } = yield call(Api.get, `${USERS_URL}`);
		
		if (errorMessage) 
			throw new Error(errorMessage);	

		const { users } = response.data;
		yield put(setUsers(users));
	} catch (error) {
		yield put(setError(error.message));
	}
}

function* watchFetchUsers() {
	yield takeEvery(UserListActionTypes.FETCH_USERS_REQUEST, fetchUsersHandler)
}

function* deleteUserHandler(action: IDeleteUserRequestAction) {
	try {
		const { error: errorMessage } = yield call(Api.delete, `${USERS_URL}/${action.userId}`);
		
		if (errorMessage) 
			throw new Error(errorMessage);	

		yield put(fetchUsers());
	} catch (error) {
		yield put(setError(error.message));
	}
}

function* watchDeleteUser() {
	yield takeEvery(UserListActionTypes.DELETE_USER_REQUEST, deleteUserHandler)
}

export default function* userListPageSagas() {
	yield all([
		watchFetchUsers(),
		watchDeleteUser()
	])
};