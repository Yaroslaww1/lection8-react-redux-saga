import axios from "axios";

class Api {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private static getError(error: any) {
    try {
      const message = error.response.data.message;
      return message;
    } catch(err) {
      return error;
    }
  }

  static post(url: string, body?: object) {
    const token = localStorage.getItem('token');
    const requestConfig = { headers: {} };
    if (token)
      requestConfig.headers = { Authorization: `Bearer ${token}` };
    return axios.post(url, body, requestConfig)
                .then(response => ({ response }))
                .catch(error => ({ error: Api.getError(error) }))
  }

  static get(url: string) {
    const token = localStorage.getItem('token');
    const requestConfig = { headers: {} };
    if (token)
      requestConfig.headers = { Authorization: `Bearer ${token}` };
    return axios.get(url, requestConfig)
                .then(response => ({ response }))
                .catch(error => ({ error: Api.getError(error) }))
  }

  static delete(url: string) {
    const token = localStorage.getItem('token');
    const requestConfig = { headers: {} };
    if (token)
      requestConfig.headers = { Authorization: `Bearer ${token}` };
    return axios.delete(url, requestConfig)
                .then(response => ({ response }))
                .catch(error => ({ error: Api.getError(error) }))
  }

  static put(url: string, body?: object) {
    const token = localStorage.getItem('token');
    const requestConfig = { headers: {} };
    if (token)
      requestConfig.headers = { Authorization: `Bearer ${token}` };
    return axios.put(url, body, requestConfig)
                .then(response => ({ response }))
                .catch(error => ({ error: Api.getError(error) }))
  }
}

export default Api;