export const getDate = (fullDateString: string) => {
  const date = new Date(fullDateString);
  const options = { weekday: 'long', month: 'long', day: 'numeric' };
  const dateString = date.toLocaleDateString('en-US', options);
  const timeString = date.toLocaleTimeString('en-US');
  return  `${dateString} ${timeString}` 
}