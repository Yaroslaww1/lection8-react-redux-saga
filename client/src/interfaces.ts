export interface IMessage {
  id: string,
  text: string,
  user: IUser
  editedAt: string,
  createdAt: string,
  isLiked: boolean
}

export interface IUser {
  id: string
  username: string
  avatar: string
  isAdmin: boolean
  password: string
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function instanceOfIUser(object: any): object is IUser {
  if (!object)
    return false;
  return ('name' in object && 'avatar' in object);
}