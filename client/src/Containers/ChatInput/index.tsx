import React, { useState } from 'react';

import "./styles.css";

interface Props {
  addMessage: (text: string) => void
}

const ChatInput: React.FC<Props> = ({
  addMessage: addMessageProps
}) => {
  const [messageBody, setMessageBody] = useState<string>('');

  const handleChange = (event: React.FormEvent<HTMLTextAreaElement>) => {
    const messageBody = event.currentTarget.value;
    setMessageBody(messageBody);
  }

  const addMessage = () => {
    addMessageProps(messageBody);
    setMessageBody('');
  }

  return (
    <div className="chat-input-wrapper">
      <textarea
        placeholder="Enter your message here"
        value={messageBody}
        onChange={handleChange}
      />
      <button
        onClick={addMessage}
      >
        Send
        <i className="fa fa-paper-plane" aria-hidden="true"></i>
      </button>
    </div>
  );
}

export default ChatInput;
