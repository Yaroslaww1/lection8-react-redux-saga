import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect, ConnectedProps } from 'react-redux';
import { RootState } from '../../store';

const mapState = (state: RootState) => ({
  isAuthorized: state.system.isAuthorized
});

const mapDispatch = {}

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;
type Props = PropsFromRedux & {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  component: any,
   // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [x:string]: any;
};

const PrivateRoute: React.FC<Props> = ({ 
  component: Component,
  isAuthorized,
  ...rest 
}) => {
  return (
    <Route
      {...rest}
      render={props => (isAuthorized
        ? <Component {...props} />
        : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />)}
    />
  )
}

export default connector(PrivateRoute);
