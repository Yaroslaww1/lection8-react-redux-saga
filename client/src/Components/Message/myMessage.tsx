import React from 'react';
import { IMessage } from '../../interfaces';
import EditIcon from '../MessageIcons/editIcon';
import MessageTime from './messageTime';

import "./styles.css";

type MyMessageProps = {
  message: IMessage,
  onEdit: () => void
}

const MyMessage: React.FC<MyMessageProps> = ({
  message,
  onEdit
}) => {
  return (
    <div className="chat-message my-message">
      <div className="message-body">
        {message.text}
        <MessageTime
          message={message}
        />
      </div>
      <div className="message-edit">
        <EditIcon
          onEdit={onEdit}
        />
      </div>
    </div>
  );
}

export default MyMessage;
