import React from 'react';
import { getDate } from '../../helpers/timeHelpers';
import { IMessage } from '../../interfaces';

import "./styles.css";

type MessageTimeProps = {
  message: IMessage,
}

const MessageTime: React.FC<MessageTimeProps> = ({
  message
}) => {
  return (
    <div className="message-time">
      {message.editedAt === ""
        ? <div>created at {getDate(message.createdAt)}</div>
        : <div>edited at {getDate(message.editedAt)}</div>
      }
    </div>
  );
}

export default MessageTime;
