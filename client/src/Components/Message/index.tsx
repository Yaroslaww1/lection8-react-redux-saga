import React from 'react';
import { IMessage } from '../../interfaces';
import LikeIcon from '../MessageIcons/likeIcon';
import MessageTime from './messageTime';

import "./styles.css";

type MessageProps = {
  message: IMessage,
  onLike: () => void
}

const Message: React.FC<MessageProps> = ({
  message,
  onLike
}) => {
  return (
    <div className="chat-message">
      <img src={message.user.avatar} alt="User avatar"></img>
      <div className="message-body">
        {message.text}
        <MessageTime
          message={message}
        />
      </div>
      <LikeIcon
        isLiked={message.isLiked}
        onLike={onLike}
      />
    </div>
  );
}

export default Message;
