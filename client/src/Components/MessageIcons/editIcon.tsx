import React from 'react';
import PropTypes from 'prop-types';
import Icon from './icon';

import "./styles.css";

type EditIconProps = {
  onEdit: () => void
}

const EditIcon: React.FC<EditIconProps> = ({
  onEdit
}) => {
  return (
    <Icon
      iconClassName="fa fa-cog"
      onClick={onEdit}
    />
  );
}

EditIcon.propTypes = {
  onEdit: PropTypes.func.isRequired
}

export default EditIcon;
