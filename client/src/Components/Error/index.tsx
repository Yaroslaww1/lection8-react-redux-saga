import React from 'react';

import "./styles.css";

interface Props {
  error: string
}

const ErrorElement: React.FC<Props> = ({
  error
}) => {
  if (!error)
    return null;
  return (
    <div className="error">
      {error}
    </div>
  )
}

export default ErrorElement;