import React from 'react'
import { IMessage, IUser } from '../../interfaces';
import DateElement from '../DateElement';
import Message from '../Message';
import MyMessage from '../Message/myMessage';

import "./styles.css";

type ChatMainProps = {
  messages: IMessage[],
  currentUser: IUser,
  messageFunction: MessageFunctions
}

type MessageFunctions = {
  onLike: (messageId: string) => void,
  onEdit: (message: IMessage) => void
}

const getMessageElement = (currentUser: IUser) => (message: IMessage, messageFunctions: MessageFunctions) => {
  const { onLike, onEdit } = messageFunctions;
  return message.user.id === currentUser.id
    ?
    (<MyMessage
      message={message}
      onEdit={() => onEdit(message)}
      key={message.id}
    />)
    :
    (<Message
      message={message}
      onLike={() => onLike(message.id)}
      key={message.id}
    />)
}

const getDateElement = (date: Date) => {
  const currentDate = new Date();
  const currentDay = currentDate.getUTCDay();
  if (currentDay - date.getUTCDay() === 1)
    return (
      <DateElement
        dateString="Yesterday"
        key={date.getTime()}
      />
    )
  return (
    <DateElement
      dateString={date.toDateString()}
      key={date.getTime()}
    />
  )
}

const getMessagesElements = (obj: ChatMainProps) => {
  const { messages, messageFunction, currentUser } = obj;
  if (messages.length === 0)
    return;
  const elements: JSX.Element[] = [];
  const getMessage = getMessageElement(currentUser);
  const firstMessage = getMessage(messages[0], messageFunction);
  const firstDate = getDateElement(new Date(messages[0].createdAt));
  elements.push(firstDate, firstMessage);

  for (let i = 1; i < messages.length; i++) {
    const currentMessage = messages[i];
    const previousMessage = messages[i - 1];
    const currentDate = new Date(currentMessage.createdAt);
    const currentDay = currentDate.getUTCDay();
    const previousDate = new Date(previousMessage.createdAt);
    const previousDay = previousDate.getUTCDay();
    if (previousDay !== currentDay) {
      const dateElement = getDateElement(currentDate);
      elements.push(dateElement);
    }
    elements.push(getMessage(currentMessage, messageFunction));
  }

  return elements;
}

const ChatMain: React.FC<ChatMainProps> = ({
  messages,
  messageFunction,
  currentUser
}) => {
  return (
    <div className="chat-main">
      {getMessagesElements({ messages, messageFunction, currentUser })}
    </div>
  )
}

export default ChatMain;